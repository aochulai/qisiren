﻿using Nancy.Helpers;
using Nancy.ViewEngines.Razor;
using QQ2564874169.WebFx.Nancy;

namespace Website
{
    public abstract class AppView<T> : RazorView<T>
    {
        public IHtmlString Raw(string html)
        { 
            return Html.Raw(html);
        }

        public static string UrlEncode(string url)
        {
            return HttpUtility.UrlEncode(url);
        }

        public static string UrlDecode(string url)
        {
            return HttpUtility.UrlDecode(url);
        }
    }

    public abstract class AppView : AppView<dynamic>
    {

    }
}