﻿using Autofac;
using Nancy.Bootstrapper;
using Nancy.Extensions;
using QQ2564874169.Core.IoC;
using QQ2564874169.WebFx.Nancy;
using QQ2564874169.WebFx.Nancy.Responses;

namespace Website
{
    public class AppBootstrapper : NancyBootstrapper
    {
        private static void IoCRegister()
        {
            IoCHelper.AutoRegister();
        }

        protected override void ApplicationStartup(ILifetimeScope container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);

            IoCRegister();

            pipelines.OnError.AddItemToEndOfPipeline((ctx, ex) =>
            {
                if (ctx.Request.IsAjaxRequest() == false)
                    return null;
                if (ex.InnerException != null)
                    ex = ex.InnerException;
                return new JsonResponse(new {ex = ex.Message});
            });
        }
    }
}