﻿
using System.IO;
using System.Linq;

namespace Website.App
{
    public class Rules
    {
        public const int Pai_Da = 1;
        public const int Pai_Xiao = 2;
        public const int Pai_2 = 3;
        public const int Pai_1 = 4;
        public const int Pai_K = 5;
        public const int Pai_Q = 6;
        public const int Pai_J = 7;
        public const int Pai_10 = 8;
        public const int Pai_9 = 9;
        public const int Pai_8 = 10;
        public const int Pai_7 = 11;
        public const int Pai_6 = 12;
        public const int Pai_5 = 13;
        public const int Pai_4 = 14;
        public const int Pai_3 = 15;

        public static int[] YiFuPai =
        {
            Pai_Da, Pai_Xiao,
            Pai_2, Pai_2, Pai_2, Pai_2,
            Pai_1, Pai_1, Pai_1, Pai_1,
            Pai_K, Pai_K, Pai_K, Pai_K,
            Pai_Q, Pai_Q, Pai_Q, Pai_Q,
            Pai_J, Pai_J, Pai_J, Pai_J,
            Pai_10, Pai_10, Pai_10, Pai_10,
            Pai_9, Pai_9, Pai_9, Pai_9,
            Pai_8, Pai_8, Pai_8, Pai_8,
            Pai_7, Pai_7, Pai_7, Pai_7,
            Pai_6, Pai_6, Pai_6, Pai_6,
            Pai_5, Pai_5, Pai_5, Pai_5,
            Pai_4, Pai_4, Pai_4, Pai_4,
            Pai_3, Pai_3, Pai_3, Pai_3
        };

        public static bool? Check(int[] pre, int[] last)
        {
            if (last.Length != pre.Length)
                return false;
            if (Quanshi2(last))
                return null;
            if (last.Length == 1 && pre.Length == 1)
            {
                if (last.First() < 3)
                    return false;
                return DanPai(pre.First(), last.First());
            }
            if (XiangTong(pre) && XiangTong(last))
            {
                return DanPai(pre.First(), last.First());
            }
            if (Shunzi(pre) && Shunzi(last))
            {
                if (last.Length < 4)
                    return false;
                return DanPai(pre.First(), last.First());
            }
            return true;
        }

        private static bool XiangTong(int[] pais)
        {
            var p = pais.First();
            foreach (var item in pais)
            {
                if (item != p)
                    return false;
            }
            return true;
        }

        private static bool DanPai(int pre, int last)
        {
            if (pre != Pai_2 && last == Pai_2)
                return true;
            return pre - 1 == last;
        }

        private static bool Shunzi(int[] pais)
        {
            for (var i = 0; i < pais.Length - 1; i++)
            {
                if (pais[i] == Pai_Da || pais[i] == Pai_Xiao)
                    continue;
                if (pais[i] == Pai_2)
                    return false;
                if (pais[i] - 1 != pais[i + 1])
                    return false;
                if (pais[i] < Pai_1)
                    return false;
            }
            return true;
        }

        private static bool Quanshi2(int[] pais)
        {
            return pais.All(i => i == Pai_2);
        }
    }
}