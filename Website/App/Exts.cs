﻿using QQ2564874169.Core.Serialization;

namespace Website.App
{
    public static class Exts
    {
        public static string ToJson(this object obj)
        {
            return Json.ToJson(obj);
        }
    }
}