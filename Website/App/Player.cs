﻿using System;
using System.Collections.Generic;

namespace Website.App
{
    public class Player
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<int> ShowPai { get; private set; }
        public int Index { get; set; }
        public Queue<object> Commands { get; private set; }
        public bool Ready { get; set; }
        public DateTime LogTime { get; set; }

        public Player()
        {
            ShowPai = new List<int>();
            Commands = new Queue<object>();
        }

        public string GetCommandJson()
        {
            var json = Commands.ToJson();
            Commands.Clear();
            return json;
        }
    } 
}