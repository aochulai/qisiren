﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class HomeVo
    {
        public int Count { get; set; }
        public bool Started { get; set; }
        public DateTime StartTime { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}