﻿using System;

namespace Website.Models
{
    public class GameVo
    {
        public long Pid { get; set; }
        public Guid Gid { get; set; }
    }
}